﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using McKesson.IHub.Plugin.SFTP.SFTPApi;
using System.Collections.Generic;
using Apprenda.SaaSGrid.Addons;
namespace McKesson.IHub.Plugin.SFTP.Tests
{
    [TestClass]
    public class SFTPTest
    {
        private SFTPWrapper wrapper = null;
        public UserCredInfo credinfo { get; set; }
        
        [TestInitialize]
        public void TestSetUp()
        {
            credinfo = new UserCredInfo();
            credinfo.UserName = "digk95a";
            credinfo.Password = "A,2437Q;#8\"1Eh7)w";
            credinfo.HostName = "mhsihub-667.oc.mckesson.com";
            credinfo.ToProvisonUser = new UserCredInfo();
            credinfo.ToProvisonUser.UserName = "test";
            credinfo.ToProvisonUser.Password = "test";
            credinfo.ToProvisonUser.HostName = "mhsihub-667.oc.mckesson.com";
            credinfo.ToProvisonUser.DownloadFolder = "download";
            credinfo.ToProvisonUser.UploadFolder = "upload";
            wrapper = SFTPWrapper.getInstance(credinfo.ToProvisonUser);
        }

        [TestMethod]
        [TestCategory("Upload File"), TestCategory("Success Scenarios")]
        public void CheckPutFiles()
        {
            try
            {
                wrapper.PutFiles("C:\\FTP\\test.doc", "download", credinfo);
                Assert.IsTrue(true);
            }
            catch(Exception ex)
            {
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        [TestCategory("Upload File"), TestCategory("Fail Scenarios")]
        public void CheckPutFiles_Exception()
        {
            try
            {
                wrapper.PutFiles("C:\\FTP\\test1.doc", "download1", credinfo);
                Assert.IsTrue(false);
            }
            catch (Exception ex)
            {
                Assert.IsTrue(true);
            }
        }

        [TestMethod]
        [TestCategory("Download File")]
        public void CheckGetFiles()
        {
            try
            {
                wrapper.GetFiles("download/test.doc", "C:\\", credinfo);
                Assert.IsTrue(true);
            }
            catch(Exception ex)
            {
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        [TestCategory("Download File"),TestCategory("Download File_WithException")]
        public void CheckGetFiles_Exception()
        {
            try
            {
                wrapper.GetFiles("test.doc", "C:\\", credinfo);
                Assert.IsTrue(false);
            }
            catch (Exception ex)
            {
                Assert.IsTrue(true);
            }
        }
        
        [TestMethod]
        [TestCategory("List File"), TestCategory("List File Succes")]
        public void ListFiles()
        {
            try
            {
                List<string> lstStrin = wrapper.ListFiles();
                Assert.IsNotNull(lstStrin);
            }
            catch(Exception ex)
            {
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        [TestCategory("List File"), TestCategory("List File exception")]
        public void ListFiles_Exception()
        {
            try
            {
                List<string> lstStrin = wrapper.ListFiles("/download/download");
                Assert.IsNull(lstStrin);
            }
            catch (Exception ex)
            {
                Assert.IsTrue(true);
            }
        }

        [TestMethod]
        [TestCategory("Delete File"), TestCategory("Delete File Succes")]
        public void DeleteFiles()
        {
            try
            {
                wrapper.DeleteFile("download/test.doc");
                Assert.IsTrue(true);
            }
            catch(Exception ex)
            {
                Assert.IsTrue(false);
            }
        }
      
        [TestMethod]
        [TestCategory("Delete File"), TestCategory("Delete File Exception")]
        public void DeleteFiles_Exception()
        {
            try
            {
                wrapper.DeleteFile("download/test1.doc");
                Assert.IsTrue(false);
            }
            catch(Exception ex)
            {
                Assert.IsTrue(true);
            }
        }

         [TestMethod]
        [TestCategory("Make Directory"), TestCategory("Make Directory Succes")]
        public void MakeDir()
        {
            try
            {
                wrapper.CreateDirectory("download/Test");
                Assert.IsTrue(true);
            }
            catch(Exception ex)
            {
                Assert.IsTrue(false);
            }
        }

         [TestMethod]
        [TestCategory("Make Directory"), TestCategory("Make Directory Exception")]
        public void MakeDir_Exception()
        {
            try
            {
                wrapper.CreateDirectory("download/Test");
                Assert.IsTrue(false);
            }
            catch(Exception ex)
            {
                Assert.IsTrue(true);
            }
        }


          [TestMethod]
        [TestCategory("remove Directory"), TestCategory("Make Directory Success")]
         public void RmDir_Exception()
        {
            try
            {
                wrapper.RemoveDirectory("download/Test1");
                Assert.IsTrue(false);
            }
            catch(Exception ex)
            {
                Assert.IsTrue(true);
            }
        }
          [TestMethod]
        [TestCategory("remove Directory"), TestCategory("Make Directory Exception")]
        public void RmDir()
        {
            try
            {
                wrapper.RemoveDirectory("download/Test");
                Assert.IsTrue(true);
            }
            catch(Exception ex)
            {
                Assert.IsTrue(false);
            }
        }
    }
}
