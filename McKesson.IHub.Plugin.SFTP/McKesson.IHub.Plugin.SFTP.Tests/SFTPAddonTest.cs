﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Apprenda.SaaSGrid.Addons;
using McKesson.IHub.Plugin.SFTP.SFTPApi;


namespace McKesson.IHub.Plugin.SFTP.Tests
{
    [TestClass]
    public class SFTPAddonTest
    {
        McKesson.IHub.Plugin.SFTP.AddOn.SFTPAddon addon = null;
        AddonProvisionRequest req = null;
        AddonProvisionRequest req1 = null;
        AddonDeprovisionRequest dereq = null;
        AddonDeprovisionRequest dereq1 = null;
        private SFTPWrapper wrapper = null;
        public UserCredInfo credinfo { get; set; }

        [TestInitialize]
        public void SFTPAddonTestIntialize()
        {
            addon = new AddOn.SFTPAddon();
            req = new AddonProvisionRequest();
            req.DeveloperOptions = "username=newuser1,password=newpasword,hostname=mhsihub.oc.mckesson.com";
            req.Manifest = new AddonManifest();
            req.Manifest.ProvisioningLocation = "mhsihub-667.oc.mckesson.com";
            req.Manifest.ProvisioningPassword = "A,2437Q;#8\"1Eh7)w";
            req.Manifest.ProvisioningUsername = "digk95a";

            dereq = new AddonDeprovisionRequest();
            dereq.DeveloperOptions = "username=newuser1,password=newpasword,hostname=mhsihub.oc.mckesson.com";
            dereq.Manifest = new AddonManifest();
            dereq.Manifest.ProvisioningLocation = "mhsihub-667.oc.mckesson.com";
            dereq.Manifest.ProvisioningPassword = "A,2437Q;#8\"1Eh7)w";
            dereq.Manifest.ProvisioningUsername = "digk95a";

            req1 = new AddonProvisionRequest();
            req1.DeveloperOptions = "username=newuser1,password=newpasword,hostname=mhsihub.oc.mckesson.com";
            req1.Manifest = new AddonManifest();
            req1.Manifest.ProvisioningLocation = "mhsihub-667.oc.mckesson.com";
            req1.Manifest.ProvisioningPassword = "A,2437Q;#8\"1Eh7)w";
            req1.Manifest.ProvisioningUsername = "digk95as";


            dereq1 = new AddonDeprovisionRequest();
            dereq1.DeveloperOptions = "username=newuser22,password=newpasword,hostname=mhsihub.oc.mckesson.com";
            dereq1.Manifest = new AddonManifest();
            dereq1.Manifest.ProvisioningLocation = "mhsihub-667.oc.mckesson.com";
            dereq1.Manifest.ProvisioningPassword = "A,2437Q;#8\"1Eh7)w";
            dereq1.Manifest.ProvisioningUsername = "digk95a";
        }

        [TestMethod]
        [TestCategory("Add New User")]
        public void CheckAddUsers()
        {
            try
            {
                var result = addon.Provision(req);
                if (result.EndUserMessage == "Successfully Provisioned")
                {
                    Assert.IsTrue(true);
                }
                else
                {
                    Assert.IsTrue(false);
                }
             
            }
            catch(Exception ex)
            {
                Assert.IsTrue(false);
            }
        }


        [TestMethod]
        [TestCategory("Add New User")]
        public void CheckAddUsers_Exception()
        {
            try
            {
                var result = addon.Provision(req1);
                if (result.EndUserMessage == "Successfully Provisioned")
                {
                    Assert.IsTrue(false);
                }
                else
                {
                    Assert.IsTrue(true);
                }

            }
            catch (Exception ex)
            {
                Assert.IsTrue(true);
            }
        }

        [TestMethod]
        [TestCategory("delete user")]
        public void CheckDeleteUsers()
        {
            try
            {
                var result = addon.Deprovision(dereq);
                if (result.IsSuccess)
                    Assert.IsTrue(true);
                else
                    Assert.IsTrue(false);
                
            }
            catch(Exception ex)
            {
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        [TestCategory("delete user")]
        public void CheckDeleteUsers_Exception()
        {
            try
            {
                
                var result = addon.Deprovision(dereq1);
                if (result.IsSuccess)
                    Assert.IsTrue(false);
                else
                    Assert.IsTrue(true);

            }
            catch (Exception ex)
            {
                Assert.IsTrue(true);
            }
        }

      
    }
}
