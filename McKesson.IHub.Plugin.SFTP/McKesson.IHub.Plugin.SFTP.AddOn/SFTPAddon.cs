﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Apprenda.SaaSGrid.Addons;
using McKesson.IHub.Plugin.SFTP.SFTPApi;


namespace McKesson.IHub.Plugin.SFTP.AddOn
{
    public class SFTPAddon : AddonBase
    {
        public override ProvisionAddOnResult Provision(AddonProvisionRequest request)
        {
            ProvisionAddOnResult result = null;
            string developerOptions = request.DeveloperOptions ?? string.Empty;
            UserCredInfo userInfo = parseAuthentication(developerOptions);
            string EndUserMessage = "FAILED: PROVISION The developerOptions parameter must contain the username and password to assign to this developer's Ftp Server." +
                   "The correct format is: username=<username>,password=<password>,hostname=<hostname>";

            userInfo.HostName = request.Manifest.ProvisioningLocation;
            userInfo.Password = request.Manifest.ProvisioningPassword;
            userInfo.UserName = request.Manifest.ProvisioningUsername;

            ProvisionResult rs = new SFTPAddonWrapper().ProvisionAddOn(userInfo);

            if (!rs.IsSuccess)
                result = new ProvisionAddOnResult(rs.Message, false, EndUserMessage);                
            else
                result = new ProvisionAddOnResult(rs.Message, true, "Successfully Provisioned");                

            return result;
       }       
        public override OperationResult Deprovision(AddonDeprovisionRequest request)
        {
            OperationResult result = null;
            string developerOptions = request.DeveloperOptions ?? string.Empty;
            UserCredInfo userInfo = parseAuthentication(developerOptions);
            string EndUserMessage = "FAILED: PROVISION The developerOptions parameter must contain the username and password to assign to this developer's Ftp Server." +
                   "The correct format is: username=<username>,password=<password>,hostname=<hostname>";

            userInfo.HostName = request.Manifest.ProvisioningLocation;
            userInfo.Password = request.Manifest.ProvisioningPassword;
            userInfo.UserName = request.Manifest.ProvisioningUsername;
            DeProvisionResult rs = new SFTPAddonWrapper().DeProvisionAddOn(userInfo);

            if (!rs.IsSuccess)
                result = new OperationResult { IsSuccess = rs.IsSuccess, EndUserMessage = EndUserMessage };
            else
                result = new OperationResult { IsSuccess = rs.IsSuccess, EndUserMessage = rs.Message };

            return result;

        }
        public override OperationResult Test(AddonTestRequest request)
        {
            var results = new OperationResult();
            try
            {
                // test input parameters
                if (string.IsNullOrWhiteSpace(request.Manifest.ProvisioningUsername))
                {
                    throw new InvalidOperationException("Username was empty when test was called.  This is invalid.");
                }

                if (string.IsNullOrWhiteSpace(request.Manifest.ProvisioningPassword))
                {
                    throw new InvalidOperationException("Password was empty when test was called.  This is invalid.");
                }

                if (string.IsNullOrWhiteSpace(request.Manifest.ProvisioningLocation))
                {
                    throw new InvalidOperationException("Location was empty when test was called.  This is invalid.");
                }
                string developerOptions = request.DeveloperOptions ?? string.Empty;
                UserCredInfo credInfo = parseAuthentication(developerOptions);
                credInfo.UserName = request.Manifest.ProvisioningUsername;
                credInfo.Password = request.Manifest.ProvisioningPassword;
                credInfo.HostName = request.Manifest.ProvisioningLocation;
                string EndUserMessage = "FAILED: PROVISION The developerOptions parameter must contain the username and password to assign to this developer's Ftp Server." +
                       "The correct format is: username=<username>,password=<password>,hostname=<hostname>";
                
                // call the test method of the wrapper
                TestAddOnResult wrapperResult =
                    new SFTPAddonWrapper().TestAddOn(credInfo);

                results.IsSuccess = wrapperResult.IsSuccess;

                results.EndUserMessage = wrapperResult.Message;

            }
            catch (Exception ex)
            {
                results.EndUserMessage = string.Format("ADDON TEST UNEXPECTED ERROR :{0} | {1} | {2} ", ex.Message, ex.StackTrace, ex.TargetSite);
                results.IsSuccess = false;
            }
            return results;
        }


        /// <summary>
        /// helper function to parse authentication strings used by the addon
        /// </summary>
        /// <param name="authData">the authroization data as a string</param>
        /// <returns>a tuple of username and password</returns>
        private UserCredInfo parseAuthentication(string authData)
        {
            string username = "";
            string password = "";
            string hostname = "";
            UserCredInfo userInfo = new UserCredInfo();
            userInfo.ToProvisonUser = new UserCredInfo();
            foreach (var pair in authData.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
            {

                var innerPair = pair.Split("=".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                if (innerPair.Length < 2)
                {
                    continue;
                }

                switch (innerPair[0])
                {
                    case "username":
                        username = innerPair[1];
                        userInfo.ToProvisonUser.UserName = username;
                        break;
                    case "password":
                        password = innerPair[1];
                        userInfo.ToProvisonUser.Password = password;
                        break;
                    case "hostname":
                        hostname = innerPair[1];
                        userInfo.ToProvisonUser.HostName = hostname;
                        break;

                }
            }
            return userInfo;
        }

    }
}
