﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using McKesson.IHub.Plugin.SFTP.SFTPApi;

namespace McKesson.IHub.Plugin.SFTP.AddOn
{
    public class SFTPAddonWrapper
    {

        private SFTPWrapper sftpWrapper = null;
        public SFTPAddonWrapper()
        {

        }
        public ProvisionResult ProvisionAddOn(UserCredInfo userInfo)
        {

            ProvisionResult provisionResult = null;
            try
            {
                sftpWrapper = SFTPWrapper.getInstance(userInfo);
                userInfo = sftpWrapper.GetProvision();
                string jsonOutput = sftpWrapper.getJson(userInfo.ToProvisonUser);
                provisionResult = new ProvisionResult
                {
                    Message = jsonOutput,
                    IsSuccess = true
                };
            }
            catch (Exception ex)
            {
                provisionResult = new ProvisionResult
                {
                    Message = "{ errorMessage: \"Unable to create user\" }",
                    IsSuccess = false
                };
            }
            return provisionResult;
        }

        public DeProvisionResult DeProvisionAddOn(UserCredInfo userInfo)
        {
            DeProvisionResult result = null;
            try
            {
                sftpWrapper = SFTPWrapper.getInstance(userInfo);
                sftpWrapper.DeProvision(userInfo);
                string jsonOutput = sftpWrapper.getJson(userInfo.ToProvisonUser);
                result = new DeProvisionResult
                {
                    Message = jsonOutput,
                    IsSuccess = true
                };
            }
            catch (Exception ex)
            {
                result = new DeProvisionResult
                {
                    Message = "{ errorMessage: \"Unable to delete user\" }",
                    IsSuccess = false
                };
            }
            return result;
        }

        public TestAddOnResult TestAddOn(UserCredInfo userInfo)
        {
            TestAddOnResult result = null;
            try
            {
                sftpWrapper = SFTPWrapper.getInstance(userInfo);
                userInfo = sftpWrapper.GetProvision();
                string jsonOutput = sftpWrapper.getJson(userInfo.ToProvisonUser);
                result = new TestAddOnResult
                {
                    Message = jsonOutput,
                    IsSuccess = true
                };
            }
            catch (Exception ex)
            {
                result = new TestAddOnResult
                {
                    Message = "{ errorMessage: \"Unable to create user\" " + ex.Message + "}",
                    IsSuccess = false
                };
            }
            return result;
        }
    }


    public class TestAddOnResult
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }

    public class ProvisionResult
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }

    public class DeProvisionResult
    {
        public bool IsSuccess { set; get; }
        public string Message { set; get; }
    }
}
