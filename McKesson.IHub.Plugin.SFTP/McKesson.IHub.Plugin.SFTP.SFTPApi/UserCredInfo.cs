﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McKesson.IHub.Plugin.SFTP.SFTPApi
{
    public class UserCredInfo
    {
        
        public string UserName { get; set; }
        public string Password { get; set; }
        public string HostName { get; set; }
        public string PrivateKey { get; set; }
        public string PassPhrase { get; set; }
        public string UploadFolder { get; set; }
        public string DownloadFolder { get; set; }
        public string UserGroup { get; set; }

        public UserCredInfo ToProvisonUser { get; set; }
    }
}
