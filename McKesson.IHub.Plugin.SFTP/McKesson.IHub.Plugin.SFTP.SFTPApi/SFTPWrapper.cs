﻿using Newtonsoft.Json;
using System;
using sysCol = System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tamir.SharpSsh;
using Tamir.SharpSsh.java.io;
using Tamir.SharpSsh.jsch;
using Tamir.Streams;
using Tamir.SharpSsh.java.util;

namespace McKesson.IHub.Plugin.SFTP.SFTPApi
{
    public class SFTPWrapper
    {
        private JSch jsch;

        protected ChannelSftp sftpChannel { get; set; }
        protected ChannelShell shellChannel { get; set; }
        protected ChannelExec execChannel { get; set; }
        protected SshStream sshStream { get; set; }

        protected Session session { get; set; }
        protected String username { get; set; }
        protected String password { get; set; }
        protected String host { get; set; }
        protected String privateKey { get; set; }
        protected String passPhrase { get; set; }
        protected int port { get; set; }
        protected string SessionError { get; set; }
        protected UserCredInfo CredInfo { get; set; }
        protected string promptPass { get; set; }
        private volatile static SFTPWrapper instance = null;
        private static object syncRoot = new Object();

        public enum CreateInfo
        {
            ALREADY_CREATED = 1,
            SUCCESS = 2,
            FAILURE = 3
        }
        public static SFTPWrapper getInstance(UserCredInfo userInfo)
        {
            if (SFTPWrapper.instance == null)
            {
                lock (syncRoot)
                {
                    if (SFTPWrapper.instance == null)
                    {
                        SFTPWrapper.instance = new SFTPWrapper(userInfo);
                    }
                }
            }
            return SFTPWrapper.instance;
        }

        private SFTPWrapper(UserCredInfo userInfo)
        {
            jsch = new JSch();
            CredInfo = userInfo;
            username = userInfo.UserName;
            password = userInfo.Password;
            host = userInfo.HostName;
            port = 22;
            privateKey = userInfo.PrivateKey;
            passPhrase = userInfo.PassPhrase;
            promptPass = "echo -e '" + CredInfo.Password + "' |";
        }

        public void openConnection()
        {
            try
            {
                if (null == session || !session.isConnected())
                {
                    //jsch.addIdentity(userInfo.UserName, userInfo.PassPhrase);
                    session = jsch.getSession(this.username, this.host, this.port);
                    //jschSession.setUserInfo(userInfo);
                    sysCol.Hashtable config = new sysCol.Hashtable();
                    config.Add("StrictHostKeyChecking", "no");
                    session.setConfig(config);
                    session.setPassword(this.password);
                    session.connect();
                    SessionError = string.Empty;
                }
            }
            catch (Exception ex)
            {
                SessionError = ex.Message;
            }

        }
        public void SetShellChannel()
        {
            try
            {
                if (null == shellChannel || !shellChannel.isConnected())
                {

                    shellChannel = (ChannelShell)session.openChannel("shell");
                    shellChannel.setPty(true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SetSshStream()
        {
            try
            {
                if (null == sshStream)
                {

                    sshStream = new SshStream(CredInfo.HostName, CredInfo.UserName, CredInfo.Password);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void SetExecChannel()
        {
            try
            {
                if (null == execChannel || !execChannel.isConnected())
                {

                    execChannel = (ChannelExec)session.openChannel("exec");
                    execChannel.setPty(true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void SetSftpChannel()
        {
            try
            {
                if (null == sftpChannel || !sftpChannel.isConnected())
                {

                    sftpChannel = (ChannelSftp)session.openChannel("sftp");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserCredInfo GetProvision()
        {

            bool isProvisioned = false;
            try
            {
                openConnection();
                if (SessionError.Length > 0)
                {
                    throw new Exception(SessionError);
                }
                if (CredInfo.ToProvisonUser != null)
                {
                    if (string.IsNullOrEmpty(CredInfo.ToProvisonUser.UserName))
                        throw new Exception("User name cannot be empty");
                    else if (string.IsNullOrEmpty(CredInfo.ToProvisonUser.Password))
                        throw new Exception("Password cannot be empty");
                    CreateInfo groupInfo = CreateGroup();
                    if (CreateInfo.ALREADY_CREATED == groupInfo || CreateInfo.SUCCESS == groupInfo)
                    {
                        CredInfo.ToProvisonUser.UserGroup = "sftpUsers";
                        CreateInfo userCInfo = CreateUser();

                        if (CreateInfo.SUCCESS == userCInfo)
                        {
                            Mkdir();
                            CredInfo.ToProvisonUser.UploadFolder = "upload";
                            CredInfo.ToProvisonUser.DownloadFolder = "download";
                        }
                    }
                }
                else
                {
                    throw new Exception("Error while creating user");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error while connetion session" + ex.ToString());
            }
            finally
            {
                if (session != null)
                    session.disconnect();
            }
            return CredInfo;
        }

        public UserCredInfo DeProvision(UserCredInfo userInfo)
        {
            try
            {

                CreateInfo delUser = DeleteUser();
                if (CreateInfo.FAILURE == delUser)
                {
                    throw new Exception("Error while deleting user");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error while deleting user");
            }
            return userInfo;

        }


        private CreateInfo Mkdir()
        {
            string promptPass = "echo -e '" + CredInfo.Password + "' | ";

            string command = "sudo -S mkdir -m 777 /home/" + CredInfo.ToProvisonUser.UserName + "/upload" + " /home/" + CredInfo.ToProvisonUser.UserName + "/download";
            string command1 = string.Empty;
            try
            {

                if (CheckSudoNeeded())
                {
                    command1 = promptPass + command;
                }
                else
                {
                    command1 = command;
                }
                if (sshStream == null)
                    SetSshStream();
                sshStream.Write(command1);
                sshStream.Flush();
                Thread.Sleep(500);
                string str = sshStream.ReadResponse();

            }
            catch (Exception ex)
            {
                return CreateInfo.FAILURE;
            }
            return CreateInfo.SUCCESS;
        }
        private bool CheckSudoNeeded()
        {
            byte[] byteArray = new byte[8000];
            MemoryStream memStream = null;
            bool isPasswordNeeded = false;
            try
            {
                string command = "sudo -n true";
                if (sshStream == null)
                    SetSshStream();
                sshStream.Write(command);
                sshStream.Flush();
                Thread.Sleep(500);
                string str = sshStream.ReadResponse();
                if (!string.IsNullOrEmpty(str) && str.ToLower().Contains("password is required"))
                    isPasswordNeeded = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
            return isPasswordNeeded;
        }
        private CreateInfo DeleteUser()
        {
            string promptPass = "echo -e '" + CredInfo.Password + "' |";

            string command = "sudo -S  /usr/sbin/userdel -r " + CredInfo.ToProvisonUser.UserName;
            string command1 = string.Empty;
            try
            {

                if (CheckSudoNeeded())
                {
                    command1 = promptPass + command;
                }
                else
                {
                    command1 = command;
                }
                if (sshStream == null)
                    SetSshStream();
                sshStream.Write(command1);
                sshStream.Flush();
                Thread.Sleep(500);
                string str = sshStream.ReadResponse();
                if (!string.IsNullOrEmpty(str) && str.ToLower().Contains("does not exist"))
                    return CreateInfo.ALREADY_CREATED;
            }
            catch (Exception ex)
            {
                return CreateInfo.FAILURE;
            }
            return CreateInfo.SUCCESS;
        }
        public CreateInfo CreateGroup()
        {
            string promptPass = "echo -e '" + CredInfo.Password + "' |";

            string command = "sudo -S  /usr/sbin/groupadd sftpusers";
            string command1 = string.Empty;
            try
            {

                if (CheckSudoNeeded())
                {
                    command1 = promptPass + command;
                }
                else
                {
                    command1 = command;
                }
                if (sshStream == null)
                    SetSshStream();
                sshStream.Write(command1);
                sshStream.Flush();
                Thread.Sleep(500);
                string str = sshStream.ReadResponse();
                if (!string.IsNullOrEmpty(str) && str.ToLower().Contains("already exists"))
                    return CreateInfo.ALREADY_CREATED;
            }
            catch (Exception ex)
            {
                return CreateInfo.FAILURE;
            }
            return CreateInfo.SUCCESS;
        }

        public CreateInfo CreateUser()
        {

            string promptPass = "echo -e '" + CredInfo.Password + "' |";

            string command = "sudo -S  /usr/sbin/useradd " + CredInfo.ToProvisonUser.UserName + " -g sftpusers -p  $(openssl passwd -1 " + CredInfo.ToProvisonUser.Password + ")";
            string command1 = string.Empty;
            try
            {

                if (CheckSudoNeeded())
                {
                    command1 = promptPass + command;
                }
                else
                {
                    command1 = command;
                }
                if (sshStream == null)
                    SetSshStream();
                sshStream.Write(command1);
                sshStream.Flush();
                Thread.Sleep(500);
                string str = sshStream.ReadResponse();
                if (!string.IsNullOrEmpty(str) && str.ToLower().Contains("already exists"))
                    return CreateInfo.ALREADY_CREATED;
            }
            catch (Exception ex)
            {
                return CreateInfo.FAILURE;
            }
            return CreateInfo.SUCCESS;
        }

        public string getJson(UserCredInfo userInfo)
        {
            string jsonObj = string.Empty;
            try
            {
                if (userInfo != null)
                {
                    //jsonObj = JsonConvert.SerializeObject(userInfo);
                    jsonObj = string.Format("UserName={0},Password={1},HostName={2},UploadFolder={3},DownloadFolder={4}", userInfo.UserName, userInfo.Password, userInfo.HostName
                        , userInfo.UploadFolder == null ? string.Empty : userInfo.UploadFolder, userInfo.DownloadFolder == null ? string.Empty : userInfo.DownloadFolder);
                }
            }
            catch
            {

            }
            return jsonObj;
        }
        public void GetFiles(string fromLocation, string destLocation, UserCredInfo userInfo)
        {
            try
            {
                openConnection();
                SetSftpChannel();
                sftpChannel.connect();
                SftpProgressMonitor monitor = new MonitorSFTP();
                int mode = ChannelSftp.OVERWRITE;
                sftpChannel.get(fromLocation, destLocation, monitor, mode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sftpChannel != null || !sftpChannel.isConnected())
                {
                    sftpChannel.disconnect();
                }

            }

        }

        public void PutFiles(string fromLocation, string toLocation, UserCredInfo userInfo)
        {
            try
            {

                try
                {
                    openConnection();
                    SetSftpChannel();
                    sftpChannel.connect();
                    SftpProgressMonitor monitor = new MonitorSFTP();
                    int mode = ChannelSftp.OVERWRITE;
                    sftpChannel.put(fromLocation, toLocation, monitor, mode);

                }
                catch (SftpException e)
                {
                    Console.WriteLine(e.message);
                }
                finally
                {
                    if (sftpChannel != null || !sftpChannel.isConnected())
                    {
                        sftpChannel.disconnect();
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<String> ListFiles(string dir = "")
        {
            List<String> lsFiles = new List<String>();
            try
            {

                openConnection();
                SetSftpChannel();
                sftpChannel.connect();
                SftpProgressMonitor monitor = new MonitorSFTP();
                int mode = ChannelSftp.OVERWRITE;
                string home = sftpChannel.getHome();
                if (dir == "")
                {
                    sftpChannel.cd(home);
                    dir = home;
                }
                else
                {
                    dir = dir.TrimEnd(new char[] { '/', '\\' });
                    dir = home + '/' + dir;
                    sftpChannel.cd(dir);
                }

                Vector vectors = sftpChannel.ls(dir);
                foreach (ChannelSftp.LsEntry entry in vectors)
                {
                    if (!entry.getFilename().startsWith("."))
                    {
                        lsFiles.Add(entry.getFilename());
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sftpChannel != null || !sftpChannel.isConnected())
                {
                    sftpChannel.disconnect();
                }
            }
            return lsFiles;
        }

        public void CreateDirectory(string directoryName)
        {
            try
            {
                openConnection();
                SetSftpChannel();
                sftpChannel.connect();
                SftpProgressMonitor monitor = new MonitorSFTP();
                int mode = ChannelSftp.OVERWRITE;
                sftpChannel.mkdir(directoryName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sftpChannel != null || !sftpChannel.isConnected())
                {
                    sftpChannel.disconnect();
                }
            }
        }

        public void RemoveDirectory(string directoryName)
        {
            try
            {
                openConnection();
                SetSftpChannel();
                sftpChannel.connect();
                SftpProgressMonitor monitor = new MonitorSFTP();
                int mode = ChannelSftp.OVERWRITE;
                sftpChannel.rmdir(directoryName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sftpChannel != null || !sftpChannel.isConnected())
                {
                    sftpChannel.disconnect();
                }
            }
        }

        public void DeleteFile(string fileName)
        {
            try
            {
                openConnection();
                SetSftpChannel();
                sftpChannel.connect();
                SftpProgressMonitor monitor = new MonitorSFTP();
                int mode = ChannelSftp.OVERWRITE;
                sftpChannel.rm(fileName);
            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                if (sftpChannel != null || !sftpChannel.isConnected())
                {
                    sftpChannel.disconnect();
                }
            }
        }
    }
}