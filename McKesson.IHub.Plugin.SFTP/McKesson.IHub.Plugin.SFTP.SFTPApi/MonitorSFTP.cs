﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tamir.SharpSsh.jsch;

namespace McKesson.IHub.Plugin.SFTP.SFTPApi
{
    public class MonitorSFTP : SftpProgressMonitor
    {
        //private ConsoleProgressBar bar;
        private long c = 0;
        private long max = 0;
        private long percent = -1;
        int elapsed = -1;

        System.Timers.Timer timer;

        public override void init(int op, String src, String dest, long max)
        {
            //bar = new ConsoleProgressBar();
            this.max = max;
            elapsed = 0;
            timer = new System.Timers.Timer(1000);
            timer.Start();
            timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Elapsed);
        }
        public override bool count(long c)
        {
            this.c += c;
            if (percent >= this.c * 100 / max) { return true; }
            percent = this.c * 100 / max;

            string note = ("Transfering... [Elapsed time: " + elapsed + "]");

            //bar.Update((int)this.c, (int)max, note);
            return true;
        }
        public override void end()
        {
            timer.Stop();
            timer.Dispose();
            string note = ("Done in " + elapsed + " seconds!");
            //bar.Update((int)this.c, (int)max, note);
            //bar=null;
        }

        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            this.elapsed++;
        }
    }
}

